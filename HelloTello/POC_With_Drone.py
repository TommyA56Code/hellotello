from djitellopy import Tello
import cv2
import pygame
from pygame.locals import *
import numpy as np
import time, re
import speech_recognition as sr
import pyttsx3 as voice

# Instance Vars
FPS = 25
font = pygame.font.SysFont('comicsansms', 20)
recon = sr.Recognizer()
mic = sr.Microphone()
engine = voice.init()
engine.setProperty('rate', 125)
voices = engine.getProperty('voices')
engine.setProperty('voice', voices[1].id)

# Initialze Pygame
pygame.init()

# Create pygame window
pygame.display.set_caption("Tello-Game video stream")
screen = pygame.display.set_mode([960, 720])

# Create tello object to interact with called tello
tello = Tello()

# Master Movement Velocities
speed = 0
ForwardsBackwards = 0
movRightLeft = 0
UpDown = 0
rotateYaw = 0

send_rc_control = True

# create update timer
pygame.time.set_timer(USEREVENT + 1, 50)


def run():
	if not tello.connect():
		print("Tello not connected")
		return

	# In case streaming is on. This happens when we quit this program without the escape key.
	if not tello.streamoff():
		print("Could not stop video stream")
		return

	if not tello.streamon():
		print("Could not start video stream")
		return
	frame_read = tello.get_frame_read()

	stop = False
	speed = 60
	while stop != True:
		words = get_words()
		print(words)

		try:
			number = re.findall(r'\d+', words)
			number = str(number[0])
		except IndexError:
			number = 2  # default number
			print("No Number")

		# Reset Movement Vels
		if UpDown > 0:
			UpDown = 0
			update()
		elif UpDown < 0:
			UpDown = 0
			update()

		if 'up' in words:
			go_up(number)
		elif 'down' in words:
			go_down(number)
		# elif 'left' in words:
		# 	left_right_forwards_backwards(number, 'left')
		# elif 'right' in words:
		# 	left_right_forwards_backwards(number, 'right')
		# elif 'forward' in words:
		# 	left_right_forwards_backwards(number, 'forwards')
		# elif 'backward' in words:
		# 	left_right_forwards_backwards(number, 'backwards')
		# elif words == 'rotate negative':
		# 	rotate('left')
		# elif words == 'rotate positive':
		# 	rotate('right')
		# elif 'battery' in words:
		# 	batteryCheck()
		# elif 'altitude' in words:
		# 	altitudeCheck()
		# elif 'set' in words:  # set Speed
		# 	speed = number
		# elif 'get speed' in words:
		# 	print(speed)
		# elif 'land' in words:
		# 	land()
		# elif 'take' in words:
		# 	take_off()
		elif 'exit' in words:
			engine.say('Good Bye')
			engine.runAndWait()
			stop = True

		screen.fill([0, 0, 0])
		frame = cv2.cvtColor(frame_read.frame, cv2.COLOR_BGR2RGB)
		frame = np.rot90(frame)
		frame = np.flipud(frame)
		frame = pygame.surfarray.make_surface(frame)
		screen.blit(frame, (0, 0))
		#batteryDisp = font.render(battery, True, (255, 0, 0))  # Add the battery display to the screen
		#screen.blit(batteryDisp, (50, 10))
		#altDisp = font.render(alt, True, (255, 0, 0))  # Add Alt display to screen
		#screen.blit(altDisp, (50, 40))
		pygame.display.update()

		time.sleep(1 / FPS)
		# Call it always before finishing. I deallocate resources.
		tello.end()


def get_words():
	try:
		with mic as source:
			recon.adjust_for_ambient_noise(source)
			print('Speak!')
			audio = recon.listen(source)

		print('Processing...\n')
		words = recon.recognize_google(audio)
		return words
	except sr.UnknownValueError:
		return 'Try Again.'


def go_up(y):  # y is int for how much to climb or descend
	UpDown = speed
	update()


def go_down(y):
	UpDown = -speed
	update()


def update():
	if send_rc_control:
		tello.send_rc_control(movRightLeft, ForwardsBackwards, UpDown, rotateYaw)


def main():
	run()


if __name__ == '__main__':
	main()
