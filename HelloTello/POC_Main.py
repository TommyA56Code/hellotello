import speech_recognition as sr
import pyttsx3 as voice
import re


recon = sr.Recognizer()
mic = sr.Microphone()
engine = voice.init()
engine.setProperty('rate', 125)
voices = engine.getProperty('voices')
engine.setProperty('voice', voices[1].id)


def run():
		stop = False
		speed = 60
		while stop != True:
			words = getWords()
			print(words)

			try:
				number = re.findall(r'\d+', words)
				number = str(number[0])
			except IndexError:
				number = 2 #default number
				print("No Number")

			if 'up' in words:
				goUp(number)
			elif 'down' in words:
				goDown(number)
			elif 'left' in words:
				left_right_forwards_backwards(number,'left')
			elif 'right' in words:
				left_right_forwards_backwards(number,'right')
			elif 'forward' in words:
				left_right_forwards_backwards(number,'forwards')
			elif 'backward' in words:
				left_right_forwards_backwards(number,'backwards')
			elif words == 'rotate negative':
				rotate('left')
			elif words == 'rotate positive':
				rotate('right')
			elif 'battery' in words:
				batteryCheck()
			elif 'altitude' in words:
				altitudeCheck()
			elif 'set' in words: #set Speed
				speed = number
			elif 'get speed' in words:
				print(speed)
			elif 'land' in words:
				land()
			elif 'take' in words:
				take_off()
			elif 'exit' in words:
				engine.say('Good Bye')
				engine.runAndWait()
				stop = True


def getWords():
		try:
			with mic as source:
				recon.adjust_for_ambient_noise(source)
				print('Speak!')
				audio = recon.listen(source)
	
			print('Processing...\n')
			words = recon.recognize_google(audio)
			return words
		except sr.UnknownValueError:
			return 'Try Again.'


def goUp(y): #y is int for how much to climb or descend
		print('Climb Alt by '+ str(y))


def goDown(y):
		print("Descend by "+ str(-y))


def left_right_forwards_backwards(x, direction): # x is time in seconds to move in that direction
		if direction == 'left':
			print('Moving Left for '+ str(x) +"seconds")
		if direction == 'right':
			print('Moving Right for '+ str(x) +"seconds")
		if direction == 'forwards':
			print('Moving Forards for '+ str(x) +"seconds")
		if direction == 'backwards':
			print('Moving Backwards for '+ str(x) +"seconds")


def rotate(direction): # Rotate by 45 degreee incriments
		if direction == 'right':
			print('Rotating Right 45 degrees')
		if direction == 'left':
			print('Rotating Left 45 degrees')


def altitudeCheck():
		print('Current altitude is 10 ft')


def batteryCheck():
		print('Current battery level is 85%')


def land():
		#Tello land function call
		print('Landing')

def take_off():
		#tello take off function call
		print('Take Off')


def main():
	run()

if __name__ == '__main__':
	main()